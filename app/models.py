from app import db
from flask_login import UserMixin

OrderItem = db.Table('OrderItem', db.Model.metadata,db.Column('menuItemId',db.Integer, db.ForeignKey('menu_item.id'),primary_key=True),
db.Column('orderId',db.Integer,db.ForeignKey('order.id'),primary_key=True))
#class OrderItem(db.Model):
#    __tablename__ = 'OrderItem'
#    db.Column('menuItemId',db.Integer, db.ForeignKey('menu_item.id'),primary_key=True)
#    db.Column('orderId',db.Integer,db.ForeignKey('order.id'),primary_key=True)

#This creates a new table that can be used in the database
class User(UserMixin,db.Model):
    #This integer uniquely identifies the row
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    #This stores up to 30 characters as the username
    name = db.Column(db.String(100))
    #This stores up to 20 characters as the password
    password = db.Column(db.String(50))
    details = db.Column(db.String(300))
    isUser = db.Column(db.Boolean)
    orders = db.relationship('Order', backref='user', lazy='dynamic')
    menuItems = db.relationship('MenuItem', backref='user', lazy='dynamic')
    #orders = db.relationship('Order')

    def __repr_(self):
        return '{}{}{}{}{}'.format(self.id,self.username,self.password,self.details,self.isUser)
    

class MenuItem(db.Model):
    __tablename__ = 'menu_item'
    id = db.Column(db.Integer, primary_key=True)
    itemName = db.Column(db.String(30))
    itemDescription = db.Column(db.String(300))
    itemPrice = db.Column(db.Float)
    isStillVisible = db.Column(db.Boolean)
    storeID = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    includedIn = db.relationship('Order', secondary=OrderItem)

    def __repr__(self):
        return '{}{}{}{}{}'.format(self.id,self.itemName,self.itemDescription,self.itemPrice,self.isStillVisible)

class Order(db.Model):
    __tablename__ = 'order'
    id = db.Column(db.Integer, primary_key=True)
    inBasket = db.Column(db.Boolean)
    totalPrice = db.Column(db.Float)
    userId = db.Column(db.Integer, db.ForeignKey('user.id'))
    orderItems = db.relationship('MenuItem', secondary=OrderItem, overlaps="includedIn")
    #orderItems = db.relationship('OrderItem')

    def __repr__(self):
        return '{}{}{}'.format(self.id,self.inBasket,self.totalPrice)

#class OrderItem(db.Model):
#    __tablename__ = 'order_item'
#    count = db.Column(db.Integer)
#    menuItemId = db.Column(db.Integer, db.ForeignKey('menu_item.id'), primary_key=True)
#    Item = db.relationship('MenuItem')
#    orderId = db.Column(db.Integer, db.ForeignKey('order.id'), primary_key=True)#
#
#    def __repr__(self):
#        return '{}{}'.format(self.id,self.count)

