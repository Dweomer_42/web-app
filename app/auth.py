from flask import Blueprint,render_template,redirect,request,flash
from .forms import LoginForm, CreateAccountForm
from werkzeug.security import generate_password_hash, check_password_hash
from app import models
from . import db
from .models import User
from flask_login import login_user, login_required, logout_user

auth = Blueprint('auth', __name__)

@auth.route('/', methods=['GET','POST'])
def login():
    return render_template('login.html', pageName="Log In", title="Log in")

@auth.route('/loginUser')
def user_login():
    form = LoginForm()
    return render_template('loginUser.html', pageName="Log In", title="Log in", form = form)

@auth.route('/loginUser', methods=['POST'])
def user_login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False
    user = User.query.filter_by(email=email).filter_by(isUser=True).first()

    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again')
        return redirect('/loginUser')
    login_user(user,remember=remember)
    return redirect('/displayRestaurants')

@auth.route('/loginRestr')
def restaurant_login():
    form = LoginForm()
    return render_template('loginRestaurant.html', pageName="Log In", title="Log in", form = form)

@auth.route('/loginRestr', methods=['POST'])
def restaurant_login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False
    print(email)
    restaurant = User.query.filter_by(email=email).filter_by(isUser=False).first()
    if(not restaurant or not check_password_hash(restaurant.password,password)):
        flash('Please check your login details and try again')
        return redirect('/loginRestr')
    login_user(restaurant,remember=remember)
    return redirect('/myRestaurant')


@auth.route('/signup')
def signup_base():
    return render_template('CreateAccountBase.html', pageName="Create New Account", title="Create new account")

@auth.route('/signupUser')
def signup_user():
    form = CreateAccountForm()
    return render_template('CreateAccountUser.html', pageName="Create New Account", title="Create new account", form=form)

@auth.route('/signupUser', methods=['POST'])
def signup_user_post():
    email = request.form.get('email')
    username = request.form.get('username')
    password = request.form.get('password')
    user = User.query.filter_by(email=email).first()

    if user:
        flash('email address already exists')
        return redirect('/signupUser')
    
    new_user = User(email=email,name=username,password=generate_password_hash(password,method='sha256'), isUser=True)
    db.session.add(new_user)
    db.session.commit()
    return redirect('/loginUser')

@auth.route('/signupRestr')
def signup_restr():
    form = CreateAccountForm()
    return render_template('CreateAccountRestaurant.html', pageName="Create New Account", title="Create new account", form=form)


@auth.route('/signupRestr', methods=['POST'])
def signup_restr_post():
    email = request.form.get('email')
    username = request.form.get('username')
    password = request.form.get('password')
    restaurant = User.query.filter_by(email=email).first()

    if restaurant:
        flash('email address already exists')
        return redirect('/signupRestr')
    
    new_restaurant = User(email=email,name=username,password=generate_password_hash(password,method='sha256'),isUser=False)
    db.session.add(new_restaurant)
    db.session.commit()
    return redirect('/loginRestr')

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')
