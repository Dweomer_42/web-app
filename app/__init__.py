from flask import Flask,url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

from . import auth
app.register_blueprint(auth.auth, url_prefix='/')

from . import views
app.register_blueprint(views.views, url_prefix='/')
#db.init_app(app=app)
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.init_app(app)


from .models import User

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


migrate = Migrate(app,db,render_as_batch=True)

from app import views, models
