from flask_wtf import Form
from wtforms import TextField, BooleanField, PasswordField, TextAreaField, DecimalField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email

class LoginForm(Form):
    email = EmailField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember = BooleanField('remember')

class CreateAccountForm(Form):
    email = EmailField('email', validators=[DataRequired()])
    username = TextField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

class CreateMenuItemForm(Form):
    itemName = TextField('itemName',validators=[DataRequired()])
    itemDescription = TextAreaField('itemDescription',validators=[DataRequired()])
    itemPrice = DecimalField('itemPrice',places=2,rounding=None)

class ChangeRestaurantInfoForm(Form):
    name = TextField('name',validators=[DataRequired()])
    description = TextAreaField('description',validators=[DataRequired()])
