from flask import render_template,flash,redirect,request
from app import app
from app import db
from app import models
from .forms import LoginForm, CreateAccountForm, CreateMenuItemForm, ChangeRestaurantInfoForm
from flask_login import login_required, current_user
from flask import Blueprint
from .models import User

views = Blueprint('main',__name__)


@views.route('/displayRestaurants', methods=['GET','POST'])
@login_required
def displayRestaurants():
    if(request.method == 'POST'):
        return redirect('/logout')

    restaurants = models.User.query.filter_by(isUser=False)
    return render_template('displayRestaurants.html', title="Find a restaurant", pageName="Find a Restaurant", restaurantsToShow=restaurants)

@views.route('/viewBasket', methods=['GET','POST'])
@login_required
def viewBasket():
    if(request.method == 'POST'):
        return redirect('/logout')
    basket = models.Order.query.filter_by(userId=current_user.id).filter_by(inBasket=True)
    return render_template('viewBasket.html', title="View my baskets", pageName="View My Basket", order=basket)
    #orders should have a boolean that says whether they are the currently selected order

@views.route('/viewOrders', methods=['GET','POST'])
@login_required
def seeOrders():
    if(request.method == 'POST'):
        return redirect('/logout')
    basket = models.Order.query.filter_by(userId=current_user.id).filter_by(inBasket=False)
    return render_template('viewOrders.html', title="See my orders", pageName="See My Orders",orders=basket)
    #We want to be able to say the restaurant, every thing that was bought, the total price

@views.route('/<id>/menu', methods=['GET','POST'])
@login_required
def displayMenu(id):
    if(request.method == 'POST'):
        if request.form.get("myButton"):
            itemToAdd = models.MenuItem.query.get(request.form.get("myButton"))
            isOpenBasket = models.Order.query.filter_by(userId=current_user.id).filter_by(inBasket=True).first()
            if isOpenBasket:
                #alreadyFoundChecker = models.Order.query.filter_by(userId=current_user.id).filter_by(inBasket=True).get(itemToAdd.id)
                #alreadyFoundChecker = isOpenBasket.orderItems.filter_by(id=itemToAdd.id)
                #if alreadyFoundChecker:
                #    flash("That item is already in your basket")
               # else:
                current_user.orders.filter_by(inBasket=True).first().orderItems.append(itemToAdd)
                current_user.orders.filter_by(inBasket=True).first().totalPrice += itemToAdd.itemPrice
                db.session.commit()
            else:
                new_order = models.Order(inBasket=True,userId=current_user.id,totalPrice=itemToAdd.itemPrice)
                current_user.orders.append(new_order)
                db.session.add(new_order)
                db.session.commit()


        else:
            return redirect('/logout')
    menu = models.User.query.filter_by(id=id).first().menuItems
    return render_template('displayMenu.html', title="Menu", pageName="Menu",menu=menu)

#add a page for a specific menu item so that it can be added to the order

@views.route('/myRestaurant', methods=['GET','POST'])
@login_required
def displayMyRestaurant():
    if(request.method == 'POST'):
        return redirect('/logout')
    if(models.User.query.filter_by(isUser=False).filter_by(email=current_user.email)):
        return render_template('myRestaurant.html', title="My Restaurant", pageName="My Restaurant", restaurantToShow=current_user)
    

@views.route('/changeMenu', methods=['GET','POST'])
@login_required
def changeMenu():
    form = CreateMenuItemForm()
    if(form.validate_on_submit()):
        new_item = models.MenuItem(itemName=form.itemName.data,itemDescription=form.itemDescription.data,itemPrice=form.itemPrice.data,isStillVisible=True,storeID=current_user.id)
        current_user.menuItems.append(new_item)
        db.session.add(new_item)
        db.session.commit()
        return redirect('/changeMenu')
    elif(request.method == 'POST'):
        return redirect('/logout')
    restaurant=models.User.query.filter_by(isUser=False).filter_by(email=current_user.email).first()
    if(restaurant):
        menu = restaurant.menuItems
        return render_template('changeMenu.html',title="Add to Menu", pageName="Add to Menu", menu=menu, form=form)

@views.route('/changeInfo', methods=['GET','POST'])
@login_required
def changeRestaurantInfo():
    form = ChangeRestaurantInfoForm()
    if(form.validate_on_submit()):
        current_user.name = form.name.data
        current_user.details = form.description.data
        db.session.commit()
        return redirect('/myRestaurant')
    elif(request.method == 'POST'):
        return redirect('/logout')
    restaurant=models.User.query.filter_by(isUser=False).filter_by(email=current_user.email).first()
    if(restaurant):
        menu = restaurant.menuItems
        return render_template('changeRestaurantInfo.html',title="Change Restaurant Info", pageName="Change Restaurant Info", menu=menu, form=form)
#This I can put JQuery in to make it display the menu, if you choose to add a menu item it's going to show the form for making it
#Could have it so that you can just remove the item OR have it so that it hides them from the menu but stays in existence so it doesn't break 
#trying to find the reference to it, that would need to be handled so that it has a boolean condition in the database "stillInUse"